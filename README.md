# MESH UI CHALLENGE 

## Usage 

### Login

You can use any credentials for login. 


## Development

### Local Development 

Install `node` and `yarn`. Checkout this repository, open the project directory
and run
```
yarn install
yarn start
```

### Available Scripts

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
In the project directory, you can run:

#### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

#### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.


## Deployment

Create a docker image with 

```
sudo docker build -f ./deployment/dockerfile -t mesh-ui-challenge .
```

then you can start a docker container with
```
sudo docker run -d -p 9000:8080 mesh-ui-challenge
```

and open the site with `http://localhost:9000` in your browser.
