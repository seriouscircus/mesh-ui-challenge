import {css} from "styled-components";

const breakpoints = {
	phone: 375,
	tablet: 768,
	desktop: 992,
	large: 1500,
};

const colors = {
	white: '#FFFFFF',
	red: 'red',
	yellow: 'yellow',
	grey: 'grey',
	black: '#000000',
	error: '#FF0046',
	primary: '#2B68F3',
	inputBackground: '#EFEFEF',
};

const theme = {
	breakpoints,
	colors,
	dimensions: {},
	typography: {
		baseFont: css`
			font-size: 16px;
			line-height: ${20 / 16};
			font-family: Monospaced, sans-serif;
			font-weight: normal;
		`,
		inputFont: css`
			font-size: 16px;
			line-height: ${40 / 16};
			font-family: Monospaced, sans-serif;
			font-weight: normal;
		`,
	},
};

export default theme;
