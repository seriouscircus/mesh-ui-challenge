import { normalize } from 'polished';
import theme from './theme';
import {createGlobalStyle} from "styled-components";

const GlobalStyles = createGlobalStyle`
	${normalize()}
	html,
	body {
		${theme.typography.baseFont};
		height: 100%;
		min-height: 500px;
		min-width: 375px;
		background-color: ${theme.colors.white};
		-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;
		color: ${theme.colors.black};
		-webkit-tap-highlight-color: rgba(255, 255, 255, 0);
		touch-action: manipulation;
	}

	a {
		color: inherit;
		text-decoration: none;
		cursor: pointer;
		transition: background-color 0.1s;

		&:hover {
			text-decoration: underline;
			color: inherit;
		}
	}

	#root {
		height: 100%;
	}

	button,
	input[type='submit'],
	input[type='reset'] {
		background: none;
		color: inherit;
		border: none;
		padding: 0;
		font: inherit;
		cursor: pointer;
		outline: inherit;
	}
`;

export default GlobalStyles;
