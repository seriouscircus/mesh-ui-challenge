import React from 'react';
import theme from '../designSystem/theme';
import styled from 'styled-components';
import { transparentize } from 'polished';
import { useMeshes, MeshState } from '../context/useMeshes';

const SelectedMeshInfo = () => {
	const { allMeshes, selectedMesh: selectedMeshGuid } = useMeshes();

	if (!allMeshes || !selectedMeshGuid) {
		return null;
	}
	const selectedMesh = allMeshes.find((meshData) => meshData.guid === selectedMeshGuid);
	if (!selectedMesh) {
		console.error('selected mesh is not part of mesh list');
		return null;
	}
	return (
		<StyledSelectedMeshInfo>
			<div>
				<b>Name</b>: {selectedMesh.name}
			</div>
			<div>
				<b>GUID</b>: {selectedMesh.guid}
			</div>
			<div>
				<b>State</b>:
				<div
					className="mesh-state"
					style={
						{
							'--color':
								selectedMesh.state === MeshState.outOfTolerance
									? theme.colors.black
									: theme.colors.white,
							'--backgroundColor': transparentize(
								0.3,
								selectedMesh.state === MeshState.outOfTolerance
									? theme.colors.yellow
									: selectedMesh.state === MeshState.missing
									? theme.colors.red
									: theme.colors.grey
							),
						} as any
					}
				>
					{selectedMesh.state}
				</div>
			</div>
		</StyledSelectedMeshInfo>
	);
};

export const StyledSelectedMeshInfo = styled('div')`
	box-sizing: border-box;
	line-height: 1.8;
	user-select: none;
	pointer-events: none;

	> div {
		white-space: nowrap;
	}

	b {
		font-weight: bold;
	}

	.mesh-state {
		display: inline-block;
		box-sizing: border-box;
		padding: 0 12px;
		margin-left: 4px;
		color: var(--color);
		background-color: var(--backgroundColor);
		border-radius: 2px;
	}
`;

export default SelectedMeshInfo;
