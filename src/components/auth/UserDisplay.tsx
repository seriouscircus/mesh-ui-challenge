import React from 'react';
import styled from 'styled-components';
import Button, { StyledButton } from '../common/Button';
import { useAuth } from '../../context/useAuth';
import SvgAstronaut from '../../designSystem/icons/SvgAstronaut';

const UserDisplay = React.memo(() => {
	const { user, logOut } = useAuth();
	return (
		<StyledUserDisplay>
			<SvgAstronaut />
			<div>{user!.name}</div>
			<Button onClick={() => logOut()}>Log-Out</Button>
		</StyledUserDisplay>
	);
});

export const StyledUserDisplay = styled('div')`
	display: flex;
	align-items: center;

	> svg {
		width: 22px;
		height: 25px;
		margin-right: 8px;
	}

	${StyledButton} {
		margin-left: 30px;
	}
`;

export default UserDisplay;
