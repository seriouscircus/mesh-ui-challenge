import React, { ReactNode, useEffect } from 'react';
import { useAuth } from '../../context/useAuth';

type Props = {
	loadingContent: ReactNode;
};

const ProtectedContent = React.memo(
	({ children, loadingContent }: React.PropsWithChildren<Props>) => {
		const { user, loadUser } = useAuth();

		useEffect(() => {
			loadUser();
		}, []); // eslint-disable-line

		return user ? <>{children}</> : <>{loadingContent}</>;
	}
);

export default ProtectedContent;
