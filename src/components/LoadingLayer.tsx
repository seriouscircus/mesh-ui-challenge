import React, { useEffect, useRef } from 'react';
import styled from 'styled-components';
import theme from '../designSystem/theme';

type Props = {
	readonly text: string;
	readonly className?: string;
	readonly complete?: boolean;
	readonly onTransitionEnd?: VoidFunction;
};

const LoadingLayer = ({ text, className, complete, onTransitionEnd }: Props) => {
	const loadingLayerRef = useRef<HTMLDivElement | null>(null);

	useEffect(() => {
		if (loadingLayerRef.current && onTransitionEnd) {
			const element = loadingLayerRef.current;

			const handleTransitionEnd = (event: TransitionEvent) => {
				if (event.target === element) {
					onTransitionEnd();
				}
			};

			element!.addEventListener('transitionend', handleTransitionEnd);
			return () => {
				element!.removeEventListener('transitionend', handleTransitionEnd);
			};
		}
	}, [loadingLayerRef, onTransitionEnd]);

	return (
		<StyledLoadingLayer
			ref={loadingLayerRef}
			className={className}
			style={
				{
					'--opacity': complete ? 0 : 1,
				} as any
			}
		>
			<div>{text}</div>
		</StyledLoadingLayer>
	);
};

export const StyledLoadingLayer = styled('section')`
	width: 100%;
	height: 100%;
	position: absolute;
	top: 0;
	left: 0;
	background-color: ${theme.colors.white};
	display: flex;
	align-items: center;
	justify-content: center;
	opacity: var(--opacity);
	transition: opacity 0.5s linear 0.1s;

	> div {
		max-width: 300px;
		text-align: center;
		opacity: var(--opacity);
		transition: opacity 0.2s linear;
	}
`;

export default LoadingLayer;
