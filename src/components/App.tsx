import React from 'react';
import styled from 'styled-components';
import GlobalStyles from '../designSystem/GlobalStyles';
import ViewerScreen from '../screens/ViewerScreen';
import ProtectedContent from './auth/ProtectedContent';
import LoadingLayer from './LoadingLayer';
import { useAuth } from '../context/useAuth';
import LoginScreen from '../screens/auth/LoginScreen';
import LoggedOutScreen from '../screens/auth/LoggedOutScreen';
import UserDisplay, { StyledUserDisplay } from './auth/UserDisplay';

const App = React.memo(() => {
	const { shouldShowLogin, hasLoggedOut } = useAuth();
	return (
		<StyledApp>
			<GlobalStyles />
			<ProtectedContent loadingContent={<LoadingLayer text="Logging in..." />}>
				<ViewerScreen />
				<UserDisplay />
			</ProtectedContent>

			{hasLoggedOut ? <LoggedOutScreen /> : null}
			{shouldShowLogin ? <LoginScreen /> : null}
		</StyledApp>
	);
});

export const StyledApp = styled('div')`
	width: 100%;
	height: 100%;
	position: relative;

	${StyledUserDisplay} {
		position: absolute;
		top: 20px;
		right: 20px;
	}
`;

export default App;
