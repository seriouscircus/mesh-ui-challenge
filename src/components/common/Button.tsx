import React from 'react';
import styled from 'styled-components';
import theme from '../../designSystem/theme';

const Button = (props: any) => {
	const { children, onClick, disabled = false, ...restProps } = props;
	return (
		<StyledButton
			style={
				{
					'--opacity': disabled ? 0.4 : 1,
					'--pointer-events': disabled ? 'none' : '',
				} as any
			}
			onClick={(event) => {
				event.preventDefault();
				if (onClick) {
					onClick(event);
				}
			}}
			{...restProps}
		>
			{children}
		</StyledButton>
	);
};

export const StyledButton = styled('button')`
	height: 40px;
	padding: 0 20px;
	border: none;
	font: inherit;
	font-weight: bold;
	background-color: ${theme.colors.primary};
	color: ${theme.colors.white};
	border-radius: 4px;
	cursor: pointer;
	opacity: var(--opacity);
	pointer-events: var(--pointer-events);
`;

export default Button;
