import constate from 'constate';
import { useCallback, useRef, useState } from 'react';
import fetch from 'cross-fetch';
import { parse } from 'csv-parse/browser/esm/sync';
import { STLLoader } from '../utils/threeJs/stlLoader';

export type MeshData = {
	readonly guid: string;
	readonly name: string;
	readonly state: MeshState;
	readonly mesh?: any;
};

export enum MeshState {
	ok = 'ok',
	missing = 'missing',
	outOfTolerance = 'out-of-tolerance',
}

const useMeshesContext = () => {
	const [allMeshes, setAllMeshes] = useState<MeshData[]>();
	const [selectedMesh, setSelectedMesh] = useState();
	const stlLoader = useRef<any>(null);
	console.log("useMeshesContext");

	const loadAllMeshes = useCallback(async (url: string) => {
		console.log('load mesh data...');
		const csvRecords = await loadFromCsv(url);
		const allMeshes = await parseStlGeometry(csvRecords);
		console.log('...finished loading mesh data');
		setAllMeshes(allMeshes);
	}, []);

	const loadFromCsv = async (url: string): Promise<MeshData[]> => {
		console.log('load csv file');
		const response = await fetch(url);
		// TODO: handle loading errors
		const csvAsText = await response.text();
		console.log(csvAsText);
		const records = parse(csvAsText, {
			columns: true,
			skip_empty_lines: true,
			trim: true,
		});
		console.log(`found ${records.length} mesh entries`);
		return records.map(
			(record: any) =>
				({
					guid: record.Guid,
					name: record.Name,
					state: record.State,
				} as MeshData)
		);
	};

	const parseStlGeometry = async (csvRecords: MeshData[]): Promise<MeshData[]> => {
		if (!stlLoader.current) {
			stlLoader.current = new STLLoader();
		}
		return (await Promise.all(
			csvRecords.map((record) => {
				return new Promise((resolve) => {
					stlLoader.current.load(`/mesh/${record.guid}.stl`, (geometry: any) => {
						console.log(`load geometry for "${record.name}"`);
						geometry.guid = record.guid;
						resolve({
							...record,
							mesh: geometry,
						} as MeshData);
					});
					// TODO: handle parsing errors
				});
			})
		)) as MeshData[];
	};

	return {
		loadAllMeshes,
		allMeshes,
		selectedMesh,
		setSelectedMesh,
	};
};

export const [MeshesProvider, useMeshes] = constate(useMeshesContext, (value) => ({
	loadAllMeshes: value.loadAllMeshes,
	allMeshes: value.allMeshes,
	selectedMesh: value.selectedMesh,
	setSelectedMesh: value.setSelectedMesh,
}));

export default MeshesProvider;
