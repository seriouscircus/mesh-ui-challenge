import { useCallback, useState } from 'react';
import constate from 'constate';
import { sleep } from '../utils/sleep';

export type User = {
	readonly name: string;
};

const USER_STORAGE_KEY = 'user';

const useAuthContext = () => {
	const [user, setUser] = useState<User | null>(null);
	const [shouldShowLogin, setShouldShowLogin] = useState(false);
	const [hasLoggedOut, setHasLoggedOut] = useState(false);

	const loadUser = useCallback(async () => {
		if (user) {
			return;
		}
		console.log('loading user...');
		setHasLoggedOut(false);

		// simulating latency
		await sleep(600);

		const userString = sessionStorage.getItem(USER_STORAGE_KEY);
		if (userString) {
			setUser(JSON.parse(userString) as User);
			console.log('...user loaded');
		} else {
			console.log('...no user found. Show login screen.');
			setShouldShowLogin(true);
		}
	}, [user, setUser]);

	const logIn = useCallback(
		({ name, password }: { readonly name: string; readonly password: string }) => {
			console.log('log-in');
			// TODO: verify input data (skipped for simplicity here)
			const nextUser: User = {
				name,
			};
			sessionStorage.setItem(USER_STORAGE_KEY, JSON.stringify(nextUser));
			setUser(nextUser);
			setShouldShowLogin(false);
		},
		[]
	);

	const logOut = useCallback(() => {
		console.log('log-out');
		sessionStorage.clear();
		setUser(null);
		setHasLoggedOut(true);
	}, []);

	return {
		user,
		setUser,
		loadUser,
		logIn,
		logOut,
		shouldShowLogin,
		hasLoggedOut,
		setHasLoggedOut,
	};
};

export const [AuthProvider, useAuth] = constate(useAuthContext, (value) => ({
	user: value.user,
	setUser: value.setUser,
	loadUser: value.loadUser,
	logIn: value.logIn,
	logOut: value.logOut,
	shouldShowLogin: value.shouldShowLogin,
	hasLoggedOut: value.hasLoggedOut,
	setHasLoggedOut: value.setHasLoggedOut,
}));

export default AuthProvider;
