import React, { useEffect, useRef, useState } from 'react';
import SelectedMeshInfo, { StyledSelectedMeshInfo } from '../components/SelectedMeshInfo';
import { MeshState, useMeshes } from '../context/useMeshes';
import LoadingLayer from '../components/LoadingLayer';
import theme from '../designSystem/theme';
import { useThreeJsRenderer } from '../utils/useThreeJsRenderer';
import styled from 'styled-components';

const ViewerScreen = React.memo(() => {
	const containerRef = useRef<HTMLDivElement | null>(null);
	const { allMeshes, loadAllMeshes } = useMeshes();
	const [isLoadingVisible, setIsLoadingVisible] = useState(!Boolean(allMeshes));

	const { addMesh } = useThreeJsRenderer({ containerRef });

	useEffect(() => {
		(async () => {
			await loadAllMeshes('/mesh.csv');
		})();
	}, [loadAllMeshes]);

	useEffect(() => {
		if (allMeshes) {
			allMeshes.forEach((meshEntry) => {
				let color;
				switch (meshEntry.state) {
					case MeshState.outOfTolerance:
						color = theme.colors.yellow;
						break;
					case MeshState.missing:
						color = theme.colors.red;
						break;
					case MeshState.ok:
					default:
						color = theme.colors.grey;
				}
				addMesh(meshEntry.mesh, color);
			});
		}
	}, [allMeshes, addMesh]);

	return (
		<StyledViewerScreen>
			<div ref={containerRef} />
			<SelectedMeshInfo />

			{isLoadingVisible ? (
				<LoadingLayer
					text="Loading meshes..."
					complete={Boolean(allMeshes)}
					onTransitionEnd={() => setIsLoadingVisible(false)}
				/>
			) : null}
		</StyledViewerScreen>
	);
});

export const StyledViewerScreen = styled('main')`
	width: 100%;
	height: 100%;
	position: relative;

	canvas {
		display: block;
	}

	${StyledSelectedMeshInfo} {
		position: absolute;
		top: 20px;
		left: 20px;
	}
`;

export default ViewerScreen;
