import React from 'react';
import styled from 'styled-components';
import { useAuth } from '../../context/useAuth';
import theme from '../../designSystem/theme';
import Button, { StyledButton } from '../../components/common/Button';

const LoggedOutScreen = () => {
	const { loadUser } = useAuth();
	return (
		<StyledLoggedOutScreen>
			<div>
				<p>
					You have successfully logged out.
					<br />
					<Button onClick={() => loadUser()}>Login</Button>
				</p>
			</div>
		</StyledLoggedOutScreen>
	);
};

export const StyledLoggedOutScreen = styled('section')`
	width: 100%;
	height: 100%;
	position: absolute;
	top: 0;
	left: 0;
	background-color: ${theme.colors.white};
	display: flex;
	align-items: center;
	justify-content: center;

	> div {
		max-width: 300px;
		text-align: center;
	}

	${StyledButton} {
		margin-top: 10px;
	}
`;

export default LoggedOutScreen;
