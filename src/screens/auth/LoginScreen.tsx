import React, { useState } from 'react';
import styled from 'styled-components';
import { useAuth } from '../../context/useAuth';
import theme from '../../designSystem/theme';
import Button from '../../components/common/Button';

const LoginScreen = React.memo(() => {
	const [name, setName] = useState('');
	const [password, setPassword] = useState('');
	const { logIn } = useAuth();

	return (
		<StyledLoginScreen>
			<form>
				<input
					id="name"
					type="text"
					placeholder="Name"
					onChange={(event) => setName(event.target.value)}
				/>
				<input
					id="password"
					type="password"
					placeholder="Password"
					onChange={(event) => setPassword(event.target.value)}
				/>

				<Button
					type="submit"
					disabled={!name || !password}
					onClick={() => logIn({ name, password })}
				>
					LOG IN
				</Button>
			</form>
		</StyledLoginScreen>
	);
});

const StyledLoginScreen = styled('main')<{
	readonly error?: boolean;
}>`
	width: 100%;
	height: 100%;
	position: absolute;
	top: 0;
	left: 0;
	background-color: ${theme.colors.white};
	display: flex;
	align-items: center;
	justify-content: center;

	form {
		box-sizing: border-box;
		width: 250px;
		display: flex;
		flex-direction: column;
		justify-content: stretch;
	}

	input {
		${theme.typography.inputFont};
		box-sizing: border-box;
		width: 100%;
		height: 40px;
		margin-bottom: 10px;
		background-color: ${({ error }) => (error ? theme.colors.error : theme.colors.inputBackground)};
		padding: 0 20px;
		border: none;
		border-radius: 4px;
		outline: none;
		color: ${theme.colors.black};

		&::placeholder {
			${theme.typography.baseFont};
			font-weight: bold;
			text-transform: uppercase;
			font-size: 0.8em;
			line-height: 20px;
		}

		&::selection {
			background: transparent;
		}
	}
`;

export default LoginScreen;
