import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import AuthProvider from './context/useAuth';
import MeshesProvider from './context/useMeshes';

if (process.env.REACT_APP_LOG_TO_CONSOLE === 'false') {
	console.log = () => {};
}

(async () => {
	ReactDOM.render(
		<React.StrictMode>
			<AuthProvider>
				<MeshesProvider>
					<App />
				</MeshesProvider>
			</AuthProvider>
		</React.StrictMode>,
		document.getElementById('root')
	);
})();
