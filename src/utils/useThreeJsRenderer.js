import { useRef, useEffect, useCallback } from 'react';
import * as THREE from './threeJs/threeJs';
import { TrackballControls } from './threeJs/trackballControls';
import { useMeshes } from '../context/useMeshes';

export const useThreeJsRenderer = ({ containerRef }) => {
	const scene = useRef();
	const { selectedMesh, setSelectedMesh } = useMeshes();
	const visibleObjects = useRef([]);
	const invisibleObjects = useRef([]);

	useEffect(() => {
		let camera, controls, cameraTarget, renderer;
		const raycaster = new THREE.Raycaster();
		const mouse = new THREE.Vector2();
		let INTERSECTED;

		init();
		animate();

		function init() {
			console.log('init threeJs canvas');

			// Camera
			camera = new THREE.PerspectiveCamera(35, window.innerWidth / window.innerHeight, 1, 15);
			camera.position.set(-3, 7, 2);

			cameraTarget = new THREE.Vector3(0, -0.1, 0);
			camera.lookAt(cameraTarget);

			// Background
			scene.current = new THREE.Scene();
			scene.current.background = new THREE.Color(0xf0f0f0);

			// Lights
			scene.current.add(new THREE.AmbientLight(0x808080));
			addShadowedLight(1, 1, 1, 0xffffff, 1.35);

			// renderer
			renderer = new THREE.WebGLRenderer({ antialias: true });
			renderer.setPixelRatio(window.devicePixelRatio);
			renderer.setSize(window.innerWidth, window.innerHeight);

			renderer.gammaInput = true;
			renderer.gammaOutput = true;

			renderer.shadowMap.enabled = true;

			containerRef.current.appendChild(renderer.domElement);

			controls = new TrackballControls(camera, renderer.domElement);

			controls.rotateSpeed = 2.0;
			controls.zoomSpeed = 0.3;
			controls.panSpeed = 0.2;

			controls.noZoom = false;
			controls.noPan = false;

			controls.staticMoving = true;
			controls.dynamicDampingFactor = 0.3;

			controls.minDistance = 0.3;
			controls.maxDistance = 0.3 * 100;

			document.addEventListener('mousemove', onDocumentMouseMove, false);
			window.addEventListener('resize', onWindowResize, false);
			window.parent.addEventListener('keypress', keyboard);
		}

		function addShadowedLight(x, y, z, color, intensity) {
			const directionalLight = new THREE.DirectionalLight(color, intensity);
			directionalLight.position.set(x, y, z);
			scene.current.add(directionalLight);

			directionalLight.castShadow = true;

			const d = 1;
			directionalLight.shadow.camera.left = -d;
			directionalLight.shadow.camera.right = d;
			directionalLight.shadow.camera.top = d;
			directionalLight.shadow.camera.bottom = -d;

			directionalLight.shadow.camera.near = 1;
			directionalLight.shadow.camera.far = 4;

			directionalLight.shadow.mapSize.width = 1024;
			directionalLight.shadow.mapSize.height = 1024;

			directionalLight.shadow.bias = -0.001;
		}

		function onDocumentMouseMove(event) {
			event.preventDefault();

			// calculate mouse position in normalized device coordinates
			// (-1 to +1) for both components

			mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
			mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
		}

		function onWindowResize() {
			camera.aspect = window.innerWidth / window.innerHeight;
			camera.updateProjectionMatrix();

			renderer.setSize(window.innerWidth, window.innerHeight);
			controls.handleResize();
		}

		function keyboard(ev) {
			switch (ev.key || String.fromCharCode(ev.keyCode || ev.charCode)) {
				case 'h':
					if (INTERSECTED) {
						INTERSECTED.material.visible = false;
						INTERSECTED.material.needsUpdate = true;

						// remove from visible objects
						const idx = visibleObjects.current.indexOf(INTERSECTED);
						if (idx >= 0) {
							visibleObjects.current.splice(idx, 1);

							// add to invisible objects
							invisibleObjects.current.push(INTERSECTED);
						}
					}
					break;

				case 'r':
					invisibleObjects.current.forEach(function (item, idx) {
						item.material.visible = true;
						item.material.needsUpdate = true;
					});

					visibleObjects.current = visibleObjects.current.concat(invisibleObjects.current);
					invisibleObjects.current = [];
					break;

				default:
			}
		}

		function animate() {
			requestAnimationFrame(animate);
			controls.update();

			// update the picking ray with the camera and mouse position
			raycaster.setFromCamera(mouse, camera);

			// calculate objects intersecting the picking ray

			var intersects = raycaster.intersectObjects(visibleObjects.current);

			if (intersects.length > 0) {
				if (INTERSECTED !== intersects[0].object) {
					if (INTERSECTED) {
						INTERSECTED.material.emissive.setHex(INTERSECTED.currentHex);
					}
					INTERSECTED = intersects[0].object;
					INTERSECTED.currentHex = INTERSECTED.material.emissive.getHex();
					INTERSECTED.material.emissive.setHex(0x808080);

					setSelectedMesh(INTERSECTED.geometry.guid);
				}
			} else {
				if (INTERSECTED) {
					INTERSECTED.material.emissive.setHex(INTERSECTED.currentHex);
					INTERSECTED = null;
					setSelectedMesh(null);
				}
			}

			renderer.render(scene.current, camera);
		}

		return () => {
			document.removeEventListener('mousemove', onDocumentMouseMove, false);
			window.removeEventListener('resize', onWindowResize, false);
			window.parent.removeEventListener('keypress', keyboard);
		};
	}, []); // eslint-disable-line

	const addMesh = useCallback((geometry, color) => {
		const material = new THREE.MeshStandardMaterial({
			color: color,
		});

		const mesh = new THREE.Mesh(geometry, material);

		// fit into frame
		mesh.scale.multiplyScalar(0.2);
		mesh.rotation.x = -Math.PI / 2;

		// wireframe
		const geo = new THREE.EdgesGeometry(mesh.geometry); // or WireframeGeometry
		const mat = new THREE.LineBasicMaterial({ color: 0xffffff, linewidth: 2 });
		const wireframe = new THREE.LineSegments(geo, mat);
		mesh.add(wireframe);

		scene.current.add(mesh);
		visibleObjects.current.push(mesh);
	}, []);

	return {
		addMesh,
		selectedMesh,
	};
};
